<?php

namespace Code\Tests\Unit\Web;

class BrowserEncryptionTest extends \PHPUnit\Framework\TestCase
{

    public function testCryptParsing()
    {
        // This is mostly here for documentation of the new format and requires some more tests
        $encrypted = '[crypt]eyJoaW50IjoiNzQ2NTczNzQiLCJhbGciOiJYU2Fsc2EyMCIsInNhbHQiOiJmMmVlM2FiOTFmYzI0ZTdiODUxMmZhMWFjY2UwYjliNSIsIm5vbmNlIjoiMjllZGRlZDkwNTg2MDAxYjk3YTVlODc0MmQzNTVkZTdmZDQ1MTM0NWEyZTMyNWJlIiwiY2lwaGVydGV4dCI6Ijg3NjgwOWY0NDNlN2FjZDg3YTAxYTc5MGViNzIzMjI0NWI3NzRlN2QifQ==[/crypt]';
        $encrypted2 = '[crypt]eyJoaW50IjoiNzQ2NTczNzQiLCJhbGciOiJYU2Fsc2EyMCIsInNhbHQiOiI1NWZhYmI4MTgyNGJmYWQ2MDU4NTM4YmZiZWNkNTMyOSIsIm5vbmNlIjoiNzkyNWQ5M2M4ZDU5MTZmNzlmN2UzZWFiMzI0MWJiMDY2Njc0MTM2OWI1OGZiMGE2IiwiY2lwaGVydGV4dCI6IjZmZGIwNDI1NzBjMTkwMTIwOTUzODIyYzNhZTA0YzA3ZjgwNDA2NGYifQ==[/crypt]';

        $expected = '{"hint":"74657374","alg":"XSalsa20","salt":"f2ee3ab91fc24e7b8512fa1acce0b9b5","nonce":"29edded90586001b97a5e8742d355de7fd451345a2e325be","ciphertext":"876809f443e7acd87a01a790eb7232245b774e7d"}';
        $this->assertEquals($expected,$expected,'dummy test');

    }
}