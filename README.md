An open source fediverse server with a long history of innovation. The primary focus is on privacy, consent, resilience, and the corresponding improvements in online safety that this provides.

See [FEATURES](https://codeberg.org/streams/streams/src/branch/dev/FEATURES.md).

The software supports a wide range of online behaviour, from personal communications with restricted media access - all the way to fully public broadcasting. We're big on offering choice; and giving you full control of how you wish to interact and whom you wish to interact with online. The default settings tend to favor personal and private use on relatively modest hardware for close friends and family. Adjust as desired. 

This repository uses a community-driven model. This means that there are no dedicated developers working on new features or bug fixes or translations or documentation. Instead, it relies on the contributed efforts of those that choose to use it.

This software is dedicated to the public domain to the extent permissible by law and is not associated with any consumer brand or product.

### Resources

A fediverse support group exists at 
https://fediversity.site/channel/streams

Self-hosted installation is covered in
https://codeberg.org/streams/streams/src/branch/release/install/INSTALL.txt

A list of sites that permit registration (approval may be required) is located at
https://fediversity.site/communities?type=streams_repository&open=1

### Third party resources
(please refer to these locations for instruction and support):

A docker image is maintained at
https://codeberg.org/node9/streams.docker

A Yunohost install is present in
https://apps.yunohost.org/app/streams

Managed hosting is available from
https://knthost.com/streams

Language translation instructions are provided in
https://codeberg.org/streams/streams/src/branch/release/util/README



