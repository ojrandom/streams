<div class="generic-content-wrapper">
	<div class="section-title-wrapper">
		<h2>{{$title}}</h2>
	</div>
	<div class="section-content-wrapper">
		<p>
			<b><a href="uexport/activitypub">{{$activitypub}}</a></b>
		</p>
		<p>
			<b><a href="uexport/identity-key">{{$privatekey}}</a></b>
		</p>
		<b><a href="uexport/basic">{{$basictitle}}</a></b>
		<p>{{$basic}}</p>

		<p><b><a href="uexport/complete">{{$fulltitle}}</a></b></p>
		<p>{{$full}}</p>
	
		<p>{{$extra}}</p>
		<p>{{$extra2}}</p>
		<p>{{$extra3}}</p>

		<p>{{$extra4}}</p>

	</div>
</div>
