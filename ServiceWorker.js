// This file should be served from the web root to avoid scope and cookie related issues with some browsers
self.addEventListener('install', function(e) {
	console.log('install event');
});

self.addEventListener('fetch', function(e) {
	// nothing here yet

});

self.addEventListener('push', function (event) {
	if (!(self.Notification && self.Notification.permission === 'granted')) {
		return;
	}

	const sendNotification = body => {
		// you could refresh a notification badge here with postMessage API
		const title = "";

		return self.registration.showNotification(title, {
			body,
		});
	};

	if (event.data) {
		const payload = event.data.json();
		event.waitUntil(sendNotification(payload.message));
	}
});
