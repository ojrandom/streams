<?php

/** @file */

namespace Code\Daemon;

use Code\Lib\Time;

require_once('include/hubloc.php');


class Checksites implements DaemonInterface
{

    public function run(int $argc, array $argv): void
    {

        logger('checksites: start');
        $site_id = '';
        $sql_options = '';

        if (($argc > 1) && ($argv[1])) {
            $site_id = $argv[1];
        }

        if ($site_id) {
            $sql_options = " and site_url = '" . dbesc($argv[1]) . "' ";
        }

        $days = intval(get_config('system', 'sitecheckdays'));
        if ($days < 1) {
            $days = 30;
        }

        $r = q(
            "select * from site where site_dead = 0 and site_update < %s - %s and site_type = %d $sql_options ",
            db_utcnow(),
            db_quoteinterval($days . ' DAY', true),
            intval(SITE_TYPE_ZOT)
        );

        if (! $r) {
            return;
        }

        foreach ($r as $rr) {
            if (! strcasecmp($rr['site_url'], z_root())) {
                continue;
            }

            $x = ping_site($rr['site_url']);
            if ($x['success']) {
                logger('checksites: ' . $rr['site_url']);
                q(
                    "update site set site_update = '%s' where site_url = '%s' ",
                    dbesc(Time::convert()),
                    dbesc($rr['site_url'])
                );
            } else {
                logger('marking dead site: ' . $x['message']);
                q(
                    "update site set site_dead = 1 where site_url = '%s' ",
                    dbesc($rr['site_url'])
                );
            }
        }
    }
}
