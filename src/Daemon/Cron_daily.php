<?php

namespace Code\Daemon;

use Code\Lib\ServiceClass;    
use Code\Lib\Libzot;
use Code\Extend\Hook;
use Code\Lib\Time;
use Code\Storage\Stdio;

class Cron_daily implements DaemonInterface
{

    public function run(int $argc, array $argv): void
    {

        logger('cron_daily: start');

        /**
         * Cron Daily
         *
         */

        // rotate the symfony/doctrine logs - 1 level for now
        $symfony_log = 'var/log/dev.log';
        if (file_exists($symfony_log)) {
            rename($symfony_log, $symfony_log . '.1');
        }
        // if not using symfony locally, remove the rotated logs
        if (!file_exists('.env') && file_exists($symfony_log . '.1')) {
            unlink($symfony_log . '.1');
        }
        // make sure our own site record is up-to-date
        Libzot::import_site(Libzot::site_info());


        // Fire off the Cron_weekly process if it's the correct day.

        $d3 = intval(Time::convert('UTC', 'UTC', 'now', 'N'));
        if ($d3 == 7) {
            Run::Summon([ 'Cron_weekly' ]);
        }

        // once daily run birthday_updates and then expire in background

        // FIXME: add birthday updates, both locally and for xprof for use
        // by directory servers

        update_birthdays();

        // expire any read notifications over a month old

        q(
            "delete from notify where seen = 1 and created < %s - %s",
            db_utcnow(),
            db_quoteinterval('60 DAY', true)
        );

        // expire any unread notifications over a year old

        q(
            "delete from notify where seen = 0 and created < %s - %s",
            db_utcnow(),
            db_quoteinterval('1 YEAR', true)
        );

        // expire old delivery reports

        $keep_reports = intval(get_config('system', 'expire_delivery_reports'));
        if ($keep_reports === 0) {
            $keep_reports = 10;
        }

        q(
            "delete from dreport where dreport_time < %s - %s",
            db_utcnow(),
            db_quoteinterval($keep_reports . ' DAY', true)
        );

        // delete accounts that did not submit email verification within 3 days

        $r = q(
            "select * from register where password = 'verify' and created < %s - %s",
            db_utcnow(),
            db_quoteinterval('3 DAY', true)
        );
        if ($r) {
            foreach ($r as $rv) {
                q(
                    "DELETE FROM account WHERE account_id = %d",
                    intval($rv['uid'])
                );

                q(
                    "DELETE FROM register WHERE id = %d",
                    intval($rv['id'])
                );
            }
        }

        // expire any expired accounts
        ServiceClass::downgrade_accounts();

        Run::Summon([ 'Expire' ]);

        check_hublocs();
        $data = Time::convert();
        Hook::call('cron_daily', $data);

        set_config('system', 'last_expire_day', intval(Time::convert('UTC', 'UTC', 'now', 'd')));

        /**
         * End Cron Daily
         */
    }
}
