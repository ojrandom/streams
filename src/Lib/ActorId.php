<?php

namespace Code\Lib;


class ActorId
{

    protected $id;
    protected $type;

    public const ACTORID_TYPE_UNKNOWN  = 0;
    public const ACTORID_TYPE_URL      = 1;
    public const ACTORID_TYPE_DIDKEY   = 2;
    public const ACTORID_TYPE_DIDWEB   = 3;

    public function __construct($id)
    {
        $this->id = $id;
        if (str_contains($this->id, 'did:key:')) {
            $this->type = ActorId::ACTORID_TYPE_DIDKEY;
            if (str_starts_with($this->id, 'http') || str_starts_with($this->id, 'ap://')) {
                $this->id = substr($this->id, strpos($this->id, 'did:key:'));
                $this->id = substr($this->id, 0, strpos($this->id, '/') ?: null);
                $this->id = substr($this->id, 0, strpos($this->id, '?') ?: null);
                $this->id = substr($this->id, 0, strpos($this->id, '#') ?: null);
            }
        }
        elseif (str_contains($this->id, 'did:web:')) {
            $this->type = ActorId::ACTORID_TYPE_DIDWEB;
            if (str_starts_with($this->id, 'http') || str_starts_with($this->id, 'ap://')) {
                $this->id = substr($this->id, strpos($this->id, 'did:web:'));
                $this->id = substr($this->id, 0, strpos($this->id, '/') ?: null);
                $this->id = substr($this->id, 0, strpos($this->id, '?') ?: null);
                $this->id = substr($this->id, 0, strpos($this->id, '#') ?: null);
            }
        }
        elseif (str_starts_with($this->id, 'http')) {
            $this->type = ActorId::ACTORID_TYPE_URL;
        }
        else {
            $this->type = ActorId::ACTORID_TYPE_UNKNOWN;
        }
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getType()
    {
        return $this->type;
    }

}
