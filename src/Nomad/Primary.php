<?php

namespace Code\Nomad;

use Code\Lib\BaseObject;

class Primary extends BaseObject
{
    public $address;
    public $url;
    public $connections_url;
    public $follow_url;
    public $wall;
    public $followers;
    public $following;
    public $searchContent;
    public $searchTags;
    public $permissions;

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return Primary
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return Primary
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getConnectionsUrl()
    {
        return $this->connections_url;
    }

    /**
     * @param mixed $connections_url
     * @return Primary
     */
    public function setConnectionsUrl($connections_url)
    {
        $this->connections_url = $connections_url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFollowUrl()
    {
        return $this->follow_url;
    }

    /**
     * @param mixed $follow_url
     * @return Primary
     */
    public function setFollowUrl($follow_url)
    {
        $this->follow_url = $follow_url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWall()
    {
        return $this->wall;
    }

    /**
     * @param mixed $wall
     * @return Primary
     */
    public function setWall($wall)
    {
        $this->wall = $wall;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * @param mixed $followers
     * @return Primary
     */
    public function setFollowers($followers)
    {
        $this->followers = $followers;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFollowing()
    {
        return $this->following;
    }

    /**
     * @param mixed $following
     * @return Primary
     */
    public function setFollowing($following)
    {
        $this->following = $following;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSearchContent()
    {
        return $this->searchContent;
    }

    /**
     * @param mixed $searchContent
     * @return Primary
     */
    public function setSearchContent($searchContent)
    {
        $this->searchContent = $searchContent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSearchTags()
    {
        return $this->searchTags;
    }

    /**
     * @param mixed $searchTags
     * @return Primary
     */
    public function setSearchTags($searchTags)
    {
        $this->searchTags = $searchTags;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * @param mixed $permissions
     * @return Primary
     */
    public function setPermissions($permissions)
    {
        $this->permissions = $permissions;
        return $this;
    }



}
