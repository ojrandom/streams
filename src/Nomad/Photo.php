<?php

namespace Code\Nomad;

use Code\Lib\BaseObject;

class Photo extends BaseObject
{
    public $url;
    public $type;
    public $updated;

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return Photo
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Photo
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     * @return Photo
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }


}
