<?php

namespace Code\Nomad;

use Code\Lib\BaseObject;

class Channel extends BaseObject
{
    public $id;
    public $id_sig;
    public $primary_location;
    public $public_key;
    public $signing_algorithm;
    public $username;
    public $name;
    public $name_updated;
    public $photo;
    public $cover_photo;
    public $channel_role;
    public $channel_type;
    public $protocols;
    public $searchable;
    public $adult_content;
    public $comments;
    public $follow_url;
    public $profile;
    public $permissions;
    public $permissions_for;
    public $locations;
    public $site;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Channel
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdSig()
    {
        return $this->id_sig;
    }

    /**
     * @param mixed $id_sig
     * @return Channel
     */
    public function setIdSig($id_sig)
    {
        $this->id_sig = $id_sig;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrimaryLocation()
    {
        return $this->primary_location;
    }

    /**
     * @param mixed $primary_location
     * @return Channel
     */
    public function setPrimaryLocation($primary_location)
    {
        $this->primary_location = $primary_location;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPublicKey()
    {
        return $this->public_key;
    }

    /**
     * @param mixed $public_key
     * @return Channel
     */
    public function setPublicKey($public_key)
    {
        $this->public_key = $public_key;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSigningAlgorithm()
    {
        return $this->signing_algorithm;
    }

    /**
     * @param mixed $signing_algorithm
     * @return Channel
     */
    public function setSigningAlgorithm($signing_algorithm)
    {
        $this->signing_algorithm = $signing_algorithm;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return Channel
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Channel
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNameUpdated()
    {
        return $this->name_updated;
    }

    /**
     * @param mixed $name_updated
     * @return Channel
     */
    public function setNameUpdated($name_updated)
    {
        $this->name_updated = $name_updated;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     * @return Channel
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoverPhoto()
    {
        return $this->cover_photo;
    }

    /**
     * @param mixed $cover_photo
     * @return Channel
     */
    public function setCoverPhoto($cover_photo)
    {
        $this->cover_photo = $cover_photo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getChannelRole()
    {
        return $this->channel_role;
    }

    /**
     * @param mixed $channel_role
     * @return Channel
     */
    public function setChannelRole($channel_role)
    {
        $this->channel_role = $channel_role;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getChannelType()
    {
        return $this->channel_type;
    }

    /**
     * @param mixed $channel_type
     * @return Channel
     */
    public function setChannelType($channel_type)
    {
        $this->channel_type = $channel_type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProtocols()
    {
        return $this->protocols;
    }

    /**
     * @param mixed $protocols
     * @return Channel
     */
    public function setProtocols($protocols)
    {
        $this->protocols = $protocols;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSearchable()
    {
        return $this->searchable;
    }

    /**
     * @param mixed $searchable
     * @return Channel
     */
    public function setSearchable($searchable)
    {
        $this->searchable = $searchable;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdultContent()
    {
        return $this->adult_content;
    }

    /**
     * @param mixed $adult_content
     * @return Channel
     */
    public function setAdultContent($adult_content)
    {
        $this->adult_content = $adult_content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param mixed $comments
     * @return Channel
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFollowUrl()
    {
        return $this->follow_url;
    }

    /**
     * @param mixed $follow_url
     * @return Channel
     */
    public function setFollowUrl($follow_url)
    {
        $this->follow_url = $follow_url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param mixed $profile
     * @return Channel
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * @param mixed $permissions
     * @return Channel
     */
    public function setPermissions($permissions)
    {
        $this->permissions = $permissions;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPermissionsFor()
    {
        return $this->permissions_for;
    }

    /**
     * @param mixed $permissions_for
     * @return Channel
     */
    public function setPermissionsFor($permissions_for)
    {
        $this->permissions_for = $permissions_for;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * @param mixed $locations
     * @return Channel
     */
    public function setLocations($locations)
    {
        $this->locations = $locations;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param mixed $site
     * @return Channel
     */
    public function setSite($site)
    {
        $this->site = $site;
        return $this;
    }


}
