<?php

namespace Code\Nomad;

use Code\Lib\BaseObject;

class Site extends BaseObject
{
    /*
     * @param string
     * public facing url of site
     */
    public $url;
    /*
     * @param signature
     * site url signed with site private key prepended with method and period
     * eg: sha256.op007a8iiOSuXUkqsjkcLf8h75LVnf2Df71fXKjTTakaZN7bEZ9ADjjNJKY9AYsKHKHrl3OLjCBBxFxytTMzGfd4Vq-TMVXsGlR
     */
    public $site_sig;
    /*
     * @param string
     * url of Nomad post endpoint
     */
    public $post;
    /*
     * @param string
     * url of OpenWebAuth endpoint
     */
    public $openWebAuth;
    /*
      * @param string
      * url of "reverse" OpenWebAuth endpoint
      */
    public $authRedirect;
    /*
     * @param string
     * site public key PEM
     */
    public $sitekey;
    /*
     * @param array
     * array of supported transport encryption algorithms
     */
    public $encryption;
    /*
     * @param string
     * default signature algorithm transmitted as hs2019
     * draft-cavage-http-signatures
     * example: sha256 for rsa-sha256
     */
    public $signature_algorithm;
    /*
     * @param string
     * current Nomad protocol version
     * example: 12.0
     */
    public $protocol_version;
    /*
     * @param string enum
     * options: 'approve', 'open', 'closed'
     *
     */
    public $register_policy;
    /*
     * @param string enum
     * options: 'paid', 'free', 'tiered', 'private'
     */
    public $access_policy;
    /*
     * @param string
     * admin email address
     */
    public $admin;
    /*
     * @param text/x-multicode
     * description of site
     */
    public $about;
    /*
     * @param string
     * random string generated during site install to detect duplicate installations
     */
    public $sitehash;
    /*
     * @param string
     * URL of site marketing page to direct potential new members
     * if registration is not closed
     */
    public $sellpage;
    /*
     * @param string
     * site location text when choosing sites
     * example: 'California, USA'
     */
    public $location;
    /*
     * @param string
     * The human-readable name of this site
     */
    public $sitename;
    /*
     * @param string
     * URL of site logo/avatar (300x300 preferred)
     */
    public $logo;
    /*
     * deprecated: replaced with community
     */
    public $project;
    /*
     * @param string
     * Version of the software
     * Example: 23.01.31
     */
    public $version;
    /*
     * @param string
     * Name of this server community
     * default: transliterated $sitename
     * example: 'pirates' for sitename of 'Pirates of Penzance'
     *
     */
    public $community;

    /*
     * @param bool
     */

    public $hidden;


    // deprecated. Replaced by protocol_version

    public $zot;


    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return Site
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteSig()
    {
        return $this->site_sig;
    }

    /**
     * @param mixed $site_sig
     * @return Site
     */
    public function setSiteSig($site_sig)
    {
        $this->site_sig = $site_sig;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param mixed $post
     * @return Site
     */
    public function setPost($post)
    {
        $this->post = $post;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOpenWebAuth()
    {
        return $this->openWebAuth;
    }

    /**
     * @param mixed $openWebAuth
     * @return Site
     */
    public function setOpenWebAuth($openWebAuth)
    {
        $this->openWebAuth = $openWebAuth;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthRedirect()
    {
        return $this->authRedirect;
    }

    /**
     * @param mixed $authRedirect
     * @return Site
     */
    public function setAuthRedirect($authRedirect)
    {
        $this->authRedirect = $authRedirect;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSitekey()
    {
        return $this->sitekey;
    }

    /**
     * @param mixed $sitekey
     * @return Site
     */
    public function setSitekey($sitekey)
    {
        $this->sitekey = $sitekey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEncryption()
    {
        return $this->encryption;
    }

    /**
     * @param mixed $encryption
     * @return Site
     */
    public function setEncryption($encryption)
    {
        $this->encryption = $encryption;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSignatureAlgorithm()
    {
        return $this->signature_algorithm;
    }

    /**
     * @param mixed $signature_algorithm
     * @return Site
     */
    public function setSignatureAlgorithm($signature_algorithm)
    {
        $this->signature_algorithm = $signature_algorithm;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProtocolVersion()
    {
        return $this->protocol_version;
    }

    /**
     * @param mixed $protocol_version
     * @return Site
     */
    public function setProtocolVersion($protocol_version)
    {
        $this->protocol_version = $protocol_version;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegisterPolicy()
    {
        return $this->register_policy;
    }

    /**
     * @param mixed $register_policy
     * @return Site
     */
    public function setRegisterPolicy($register_policy)
    {
        $this->register_policy = $register_policy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccessPolicy()
    {
        return $this->access_policy;
    }

    /**
     * @param mixed $access_policy
     * @return Site
     */
    public function setAccessPolicy($access_policy)
    {
        $this->access_policy = $access_policy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param mixed $admin
     * @return Site
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * @param mixed $about
     * @return Site
     */
    public function setAbout($about)
    {
        $this->about = $about;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSitehash()
    {
        return $this->sitehash;
    }

    /**
     * @param mixed $sitehash
     * @return Site
     */
    public function setSitehash($sitehash)
    {
        $this->sitehash = $sitehash;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSellpage()
    {
        return $this->sellpage;
    }

    /**
     * @param mixed $sellpage
     * @return Site
     */
    public function setSellpage($sellpage)
    {
        $this->sellpage = $sellpage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     * @return Site
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSitename()
    {
        return $this->sitename;
    }

    /**
     * @param mixed $sitename
     * @return Site
     */
    public function setSitename($sitename)
    {
        $this->sitename = $sitename;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $logo
     * @return Site
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param mixed $project
     * @return Site
     */
    public function setProject($project)
    {
        $this->project = $project;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return Site
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCommunity()
    {
        return $this->community;
    }

    /**
     * @param mixed $community
     * @return Site
     */
    public function setCommunity($community)
    {
        $this->community = $community;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * @param mixed $hidden
     * @return Site
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getZot()
    {
        return $this->zot;
    }

    /**
     * @param mixed $zot
     * @return Site
     */
    public function setZot($zot)
    {
        $this->zot = $zot;
        return $this;
    }



}
