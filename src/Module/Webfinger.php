<?php

namespace Code\Module;

use Code\Lib\ActivityStreams;
use Code\Lib\ActorId;
use Code\Web\Controller;
use Code\Lib\System;
use Code\Lib\Channel;
use Code\Lib\PConfig;
use Code\Extend\Hook;
use App;

class Webfinger extends Controller
{

    public function init()
    {

        logger('webfinger from ' . $_SERVER['REMOTE_ADDR']);

        // This is a public resource with relaxed CORS policy. Close the current login session.
        session_write_close();

        header('Access-Control-Allow-Origin: *');
        $cache = (3600 * 24); // 1 day

        header("Expires: " . gmdate("D, d M Y H:i:s", time() + $cache) . " GMT");
        // Set browser cache age as $cache.  But set timeout of 'shared caches'
        // much lower in the event that infrastructure caching is present.
        $smaxage = intval($cache / 12);
        header('Cache-Control: s-maxage=' . $smaxage . '; max-age=' . $cache . ';');
        header_remove('Pragma');
        $result = [];

        if (!is_https_request()) {
            header($_SERVER['SERVER_PROTOCOL'] . ' ' . 500 . ' ' . 'Webfinger requires HTTPS');
            killme();
        }

        $resource = $_REQUEST['resource'];

        if (!$resource || !ActivityStreams::is_url($resource)) {
            http_status_exit(400, 'Bad request');
        }

        logger('webfinger: ' . $resource, LOGGER_DEBUG);

        // Determine whether to respond as a site actor
        // or a normal channel

        $site_query = false;
        if (
            strcasecmp(rtrim($resource, '/'), z_root()) === 0
            || strcasecmp($resource, 'acct:sys@' . App::get_hostname()) === 0
            || $resource === z_root() . '/channel/sys'
        ) {
            $site_query = true;
            $result['subject'] = $resource;
            $resource = z_root() . '/channel/sys';
        }

        if (str_starts_with($resource, 'tag:')) {
            $arr = explode(':', $resource);
            if (count($arr) > 3 && $arr[2] === 'zotid') {
                $hash = $arr[3];
                $channel_target = Channel::from_hash($hash);
            }
        }


        if (str_starts_with($resource, 'acct:')) {
            $channel_nickname = punify_addr(str_replace('acct:', '', $resource));
            if (strrpos($channel_nickname, '@') !== false) {
                $host = substr($channel_nickname, strrpos($channel_nickname, '@') + 1);

                // If the webfinger address points off site, redirect to the correct site

                //if (strcasecmp($host, App::get_hostname())) {
                //    goaway('https://' . $host . '/.well-known/webfinger?f=&resource=' . $resource);
                //}
                $channel_nickname = substr($channel_nickname, 0, strrpos($channel_nickname, '@'));
            }
        }
        if (str_starts_with($resource, 'http')) {
            $parsed = parse_url($resource);
            unset($parsed['fragment']);
            unset($parsed['query']);
            $resource = unparse_url($parsed);
            $actorId = new ActorId($resource);
            if ($actorId->getType() === ActorId::ACTORID_TYPE_DIDKEY) {
                $key = str_replace('did:key:', '', $actorId->getId());
                if ($key) {
                    $didchannel = q("select * from channel left join xchan on channel_hash = xchan_hash where xchan_epubkey = '%s'",
                        dbesc($key)
                    );
                    if ($didchannel) {
                        $channel_nickname = $didchannel[0]['channel_address'];
                    }
                }
            }
            else {
                $channel_nickname = str_replace(['~', '@'], ['', ''], basename($parsed['path']));
            }
        }

        if ($channel_nickname) {
            $channel_target = Channel::from_username($channel_nickname);
        }

        $isNomadic = PConfig::Get($channel_target['channel_id'], 'system', 'nomadicAP');
        $actorURI = $isNomadic
            ? Channel::getDidResolver($channel_target, true)
            : z_root() . '/channel/' . $channel_target['channel_address'];

        if ($channel_target || $site_query) {
            $h = get_locations_by_hash($channel_target['channel_hash']);

            if (!isset($result['subject'])) {
                $result['subject'] = $resource;
            }

            $aliases = [
                z_root() . '/channel/' . $channel_target['channel_address'],
                z_root() . '/~' . $channel_target['channel_address'],
                z_root() . '/@' . $channel_target['channel_address']

            ];
            if ($isNomadic) {
                $aliases[] = 'ap://' . Channel::getDid($channel_target) . '/actor';
                $aliases[] = $actorURI;
            }

            if ($h) {
                foreach ($h as $hh) {
                    $aliases[] = 'acct:' . $hh['hubloc_addr'];
                    $aliases[] = $hh['hubloc_id_url'];
                }
            }

            $result['aliases'] = [];

            $result['properties'] = [
                'http://webfinger.net/ns/name' => $site_query ? System::get_site_name() : $channel_target['channel_name'],
                'http://xmlns.com/foaf/0.1/name' => $site_query ? System::get_site_name() : $channel_target['channel_name'],
                'https://w3id.org/security/v1#publicKeyPem' => (($site_query) ? get_config('system', 'pubkey') : $channel_target['xchan_pubkey']),
            ];

            if ($site_query) {
                $aliases[] = z_root();
                $aliases[] = 'acct:sys@' . App::get_hostname();
            }

            foreach ($aliases as $alias) {
                if ($alias !== $result['subject']) {
                    $result['aliases'][] = $alias;
                }
            }

            $result['links'] = [

                [
                    'rel' => 'self',
                    'type' => 'application/activity+json',
                    'href' => (($site_query) ? z_root() : $actorURI),
                ],

                [
                    'rel' => 'self',
                    'type' => 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"',
                    'href' => (($site_query) ? z_root() : $actorURI),
                ],

                [
                    'rel' => 'http://webfinger.net/rel/avatar',
                    'type' => $channel_target['xchan_photo_mimetype'],
                    'href' => $channel_target['xchan_photo_l'],
                ],

                [
                    'rel' => 'http://webfinger.net/rel/blog',
                    'href' => z_root() . '/channel/' . $channel_target['channel_address'],
                ],

                [
                    'rel'  => 'http://purl.org/nomad',
                    'type' => 'application/x-nomad+json',
                    'href' => (($site_query) ? z_root() : z_root() . '/channel/' . $channel_target['channel_address']),
                ],

                [
                    'rel'  => 'http://purl.org/openwebauth/v1',
                    'type' => 'application/x-nomad+json',
                    'href' => z_root() . '/owa',
                ],

                [
                    'rel'  => 'http://purl.org/openwebauth/v1#redirect',
                    'type' => 'application/x-nomad+json',
                    'href' => z_root() . '/magic',
                ],

                [
                    'rel' => 'http://purl.org/zot/protocol/6.0',
                    'type' => 'application/x-zot+json',
                    'href' => (($site_query) ? z_root() : z_root() . '/channel/' . $channel_target['channel_address']),
                ],

                [
                    'rel' => 'http://ostatus.org/schema/1.0/subscribe',
                    'template' => z_root() . '/follow?url={uri}',
                ],

                [
                    'rel'  => 'http://a9.com/-/spec/opensearch/1.1/',
                    'type' => 'application/opensearchdescription+xml',
                    'href' => z_root() . '/opensearch' . (($site_query) ? '' : '/' . $channel_target['channel_address']),
                    'title' => (($site_query) ? App::get_hostname() : $channel_target['channel_address'] . '@' . App::get_hostname()),
                ],
            ];
        }

        if (!$result) {
            header($_SERVER['SERVER_PROTOCOL'] . ' ' . 404 . ' ' . 'Not found');
            killme();
        }

        $arr = ['channel' => $channel_target, 'request' => $_REQUEST, 'result' => $result];
        Hook::call('webfinger', $arr);
        json_return_and_die($arr['result'], 'application/jrd+json', true);
    }
}
