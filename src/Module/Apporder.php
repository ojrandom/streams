<?php

namespace Code\Module;

use Code\Lib\Time;
use Code\Web\Controller;
use Code\Lib\Apps;
use Code\Lib\Navbar;
use Code\Lib\PConfig;
use Code\Render\Theme;


class Apporder extends Controller
{
    public function get()
    {

        if (!local_channel()) {
            return '';
        }

        if (get_pconfig(local_channel(), 'system', 'import_system_apps') !== Time::convert('UTC', 'UTC', 'now', 'Y-m-d')) {
            Apps::import_system_apps();
            set_pconfig(local_channel(), 'system', 'import_system_apps', Time::convert('UTC', 'UTC', 'now', 'Y-m-d'));
        }

        Navbar::set_selected('Order Apps');

        $nav_apps = [];
        $navbar_apps = [];

        if (argc() > 1 && argv(1) === 'reset') {
            $baseApps = Apps::get_base_apps();
            if ($baseApps) {
                foreach ($baseApps as $app) {
                    q("delete from app where app_channel = %d and app_id = '%s'",
                        intval(local_channel()),
                        dbesc(hash('whirlpool', $app))
                    );
                }
            }

            PConfig::Delete(local_channel(), 'system', 'import_system_apps');
            PConfig::Delete(local_channel(), 'system', 'app_order');
            PConfig::Delete(local_channel(), 'system', 'app_pin_order');
            goaway(z_root() . '/apporder');
        }

        foreach (['nav_featured_app', 'nav_pinned_app'] as $l) {
            $syslist = [];
            $list = Apps::app_list(local_channel(), false, [$l]);
            if ($list) {
                foreach ($list as $li) {
                    $syslist[] = Apps::app_encode($li);
                }
            }

            Apps::translate_system_apps($syslist);

            usort($syslist, 'Code\\Lib\\Apps::app_name_compare');

            $syslist = Apps::app_order(local_channel(), $syslist, $l);

            foreach ($syslist as $app) {
                if ($l === 'nav_pinned_app') {
                    $navbar_apps[] = Apps::app_render($app, 'nav-order-pinned');
                }
                else {
                    $nav_apps[] = Apps::app_render($app, 'nav-order');
                }
            }
        }

        return replace_macros(Theme::get_template('apporder.tpl'), [
            '$arrange'     => t('Arrange Apps'),
            '$reset' => t('Reset to defaults'),
            '$header' => [t('Change order of pinned navbar apps'), t('Change order of app tray apps')],
            '$desc' => [t('Use arrows to move the corresponding app left (top) or right (bottom) in the navbar'),
                t('Use arrows to move the corresponding app up or down in the app tray')],
            '$nav_apps' => $nav_apps,
            '$navbar_apps' => $navbar_apps
        ]);
    }
}
