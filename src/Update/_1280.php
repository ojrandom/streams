<?php

namespace Code\Update;

class _1280
{
    public function run()
    {

        q("START TRANSACTION");


        if (ACTIVE_DBTYPE == DBTYPE_POSTGRES) {
            $r1 = q("ALTER TABLE hubloc ADD hubloc_resolver text DEFAULT ''");
            $r2 = q("ALTER TABLE hubloc ALTER hubloc_resolver set NOT NULL");
            $r3 = q("ALTER TABLE hubloc ADD hubloc_openwebauth text DEFAULT ''");
            $r4 = q("ALTER TABLE hubloc ALTER hubloc_openwebauth set NOT NULL");
            $r5 = q("ALTER TABLE hubloc ADD hubloc_authredirect text DEFAULT ''");
            $r6 = q("ALTER TABLE hubloc ALTER hubloc_authredirect set NOT NULL");


            $r = ($r1 && $r2 && $r3 && $r4 && $r5 && $r6);
        } else {
            $r1 = q("ALTER TABLE hubloc ADD hubloc_resolver text NOT NULL");
            $r2 = q("ALTER TABLE hubloc ADD hubloc_openwebauth text NOT NULL");
            $r3 = q("ALTER TABLE hubloc ADD hubloc_authredirect text NOT NULL");
            $r = ($r1 && $r2 && $r3);
        }

        if ($r) {
            q("COMMIT");
            return UPDATE_SUCCESS;
        }

        q("ROLLBACK");
        return UPDATE_FAILED;
    }

    public function verify()
    {
        $columns = db_columns('hubloc');
        return in_array('hubloc_resolver', $columns);
    }


}
