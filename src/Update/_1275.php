<?php

namespace Code\Update;


class _1275
{
    public function run()
    {

        q("START TRANSACTION");


        if (ACTIVE_DBTYPE == DBTYPE_POSTGRES) {
            $r1 = q("ALTER TABLE xchan ADD xchan_epubkey text DEFAULT ''");
            $r2 = q("ALTER TABLE xchan ALTER xchan_epubkey set NOT NULL");

            $r = $r1 && $r2;
        } else {
            $r = q("ALTER TABLE xchan ADD xchan_epubkey text NOT NULL");
        }

        if ($r) {
            q("COMMIT");
            return UPDATE_SUCCESS;
        }

        q("ROLLBACK");
        return UPDATE_FAILED;
    }


    public function verify()
    {
        $columns = db_columns('xchan');
        return in_array('xchan_epubkey', $columns);
    }

}
