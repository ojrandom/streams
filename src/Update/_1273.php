<?php

namespace Code\Update;

class _1273
{

    public function run()
    {
        if (ACTIVE_DBTYPE != DBTYPE_POSTGRES) {
            q("START TRANSACTION");

            $r = q("ALTER TABLE session MODIFY COLUMN sess_data MEDIUMTEXT NOT NULL");

            if ($r) {
                q("COMMIT");
                return UPDATE_SUCCESS;
            } else {
                q("ROLLBACK");
                return UPDATE_FAILED;
            }
        }
    }

    public function verify()
    {
        return true;
    }

}
