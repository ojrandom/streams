<?php

namespace Code\Update;

class _1279
{
    public function run()
    {
        q("START TRANSACTION");

        if (ACTIVE_DBTYPE == DBTYPE_POSTGRES) {
            $r1 = q('CREATE TABLE "pushsub" (
              "id" serial NOT NULL,
              "channel_id" bigint NOT NULL,
              "endpoint" text NOT NULL,
              "json" text NOT NULL,
              PRIMARY KEY ("id"),
              UNIQUE ("endpoint"))');
            $r2 = q('create index "channel_id" on pushsub ("channel_id")');
            $r = ($r1 && $r2);
        }
        else {
            $r = q("CREATE TABLE IF NOT EXISTS `pushsub` (
                `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `channel_id` int(11) NOT NULL,
                `endpoint` varchar(256) NOT NULL,
                `json` varchar(1024) NOT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `endpoint` (`endpoint`),
                KEY `channel_id` (`channel_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
            );
        }

        if ($r) {
            q("COMMIT");
            return UPDATE_SUCCESS;
        }

        q("ROLLBACK");
        return UPDATE_FAILED;
    }

    public function verify()
    {
        $columns = db_columns('pushsub');

        if ($columns && in_array('endpoint', $columns)) {
            return true;
        }
        return false;
    }
}
