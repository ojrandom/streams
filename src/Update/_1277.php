<?php

namespace Code\Update;


class _1277
{
    public function run()
    {

        q("START TRANSACTION");


        if (ACTIVE_DBTYPE == DBTYPE_POSTGRES) {
            $r1 = q("ALTER TABLE xchan DROP CONSTRAINT xchan_pkey");
            $r2 = q("ALTER TABLE xchan ADD UNIQUE (xchan_hash)");
            $r3 = q("ALTER TABLE xchan ADD xchan_id serial NOT NULL, ADD PRIMARY KEY (xchan_id)");

            $r = $r1 && $r2 && $r3;
        } else {
            $r1 = q("ALTER TABLE xchan DROP PRIMARY KEY");
            $r2 = q("ALTER TABLE xchan ADD UNIQUE (xchan_hash)");
            $r3 = q("ALTER TABLE xchan ADD xchan_id INT UNSIGNED NOT NULL AUTO_INCREMENT, ADD PRIMARY KEY (xchan_id)");
            $r = $r1 && $r2 && $r3;
        }

        if ($r) {
            q("COMMIT");
            return UPDATE_SUCCESS;
        }

        q("ROLLBACK");
        return UPDATE_FAILED;
    }


    public function verify()
    {
        $columns = db_columns('xchan');
        return in_array('xchan_id', $columns);
    }

}
